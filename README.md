# glazilla/datatable

Full JSON interface allowing to query databases through Datatable UI (supporting mySql, MariaDB or PostgreSQL).
Tested successfully with Symfony 6+, SlimFramework, Datatable 1.13.1
Possibility to filter searches on all or specifics rows.
Filter can combine (or) values with pipe separator (|), example : "john|lisa"
For datatable howto, please refer to https://datatables.net/

### Installing

```bash
composer require glazilla/datatable:dev-master
```

### Using

```php
$myDataTable = new \Glazilla\DataTable\DataTable( \Pdo $pdoInstance);
```

#### With MariaDB / MySQL

```php
echo $myDataTable->query( [ Array $columns, String $columnKey, String $tableOrview, String $where ]);   // Returns JSON String as expected by datatables
```

#### With PostgreSQL

```php
echo $myDataTable->PGquery( [ Array $columns, String $columnKey, String $tableOrview, String $where ]); // Returns JSON String as expected by datatables
```

If you wan't to search using UNACCENT postgreSQL extension, activate the extension and insert this constant in your code :
```php
define("UNACCENT_QUERY_PG", "1");
```

#### Full example for MariaDB :

```php
  public function DataJSON(RequestInterface $request, ResponseInterface $response)
  {
    $columns = ["category", "title", "id", "when"] ;
    $contenu =  $this->datatable->Query($colonnes, "id", "view_articles", "archive=1");
    $response = $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    $response = $response->write($contenu);
    return $response;
  }
```

#### To be used within DataTable/jquery :

```javascript
    sAjaxSource: '/userdata', // route to function DataJSON
    fnServerData:
    (sSource, aoData, fnCallback) => {
      $.getJSON(sSource, aoData, (json) => {
        fnCallback(json);
      })
        .fail(() => {
          myAlert({
            text: 'An error happened !',
            icon: 'alert',
            button: 'Sorry...',
          });
        });
    },
```
