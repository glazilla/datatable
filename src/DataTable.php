<?php namespace Glazilla\DataTable;
/*
 * Glazilla\DataTable 1.1.7
 * 2023-12-21 - Add constant to search with PostgreSQL using UNACCENT PG extension (has to be initialized prior tu use) : Use define("UNACCENT_QUERY_PG", "1");
 * 2022-12-04 - Add possibility to combine searches with | (or) in input fields
 * 2022-03-03 - Fixed typo
 * 2021-10-16 - Fixed search on numeric types
 * Use QueryPG for PosgreSQL
 * Use Query   for MySQL / MariaDB
 *
 * @param Array
 * @param String
 * @param String
 * @param String
 *
 * Usage : $myDataTable = new \Glazilla\DataTable\DataTable( \Pdo $pdoInstance);
 *         echo $myDataTable->query( [ Array $columns, String $columnKey, String $tableOrview, String $where ]);   // Returns JSON String to be used with
 *         echo $myDataTable->queryPG( [ Array $columns, String $columnKey, String $tableOrview, String $where ]); // Returns JSON String
 *
 * To be used with DataTable :      sAjaxSource: 'url',
 *                                  fnServerData:
 *
 * Url should return : Content-Type, application/json with string obtained from query or queryPG
*/
class DataTable {

  private \PDO $pdo;

  /**
   * @param \PDO
   **/
  public function __construct($Connexion)
  {
    $this->pdo = $Connexion;
  }

  /**
   * Method to be used with PostgreSQL
   * ---------------------------------
   * @param Array
   * @param String
   * @param String
   * @param String
   * @return String
   **/
  public function QueryPG(Array $Colonnes, String $ColonneIndex, String $Table, String $Where = "1")
  {
    mb_internal_encoding('UTF-8');

    // Get user search string
    $entree =& $_GET;

      // Pagination
      $Pagination = "";
      if ( isset( $entree['iDisplayStart'] ) && $entree['iDisplayLength'] != '-1' )
          $Pagination = " LIMIT ".intval( $entree['iDisplayLength'] )." OFFSET ".intval( $entree['iDisplayStart'] );

      // Sort
      $regleTri = array();
      if ( isset( $entree['iSortCol_0'] ) ) {
          $colonnesTri = intval( $entree['iSortingCols'] );
          for ( $i=0 ; $i<$colonnesTri ; $i++ ) {
              if ( $entree[ 'bSortable_'.intval($entree['iSortCol_'.$i]) ] == 'true' ) {
                  $regleTri[] = $Colonnes[ intval( $entree['iSortCol_'.$i] ) ]." " .($entree['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
              }
          }
      }

      if (!empty($regleTri))
          $sOrdre = " ORDER BY ".implode(", ", $regleTri);
      else
          $sOrdre = "";

      // Filtering
      $nombreColonnes = count($Colonnes);
      if (isset($entree['sSearch']))
          $entree['sSearch'] = str_replace(["'",'"'], "", $entree['sSearch']);

      if ( isset($entree['sSearch']) && $entree['sSearch'] != "" ) {
          $regleFiltrage = array();
          for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
              if ( isset($entree['bSearchable_'.$i]) && $entree['bSearchable_'.$i] == 'true' )
                  if (mb_strpos($entree['sSearch'], '|')) {
                      $orWhere = '';
                      foreach (explode('|', $entree['sSearch']) as $params) {
                          $orWhere .= $orWhere !== '' ? ' OR ' : '';
                          if (defined('UNACCENT_QUERY_PG') && UNACCENT_QUERY_PG === "1")
                            $orWhere .= "UNACCENT(CAST(" . $Colonnes[$i] . " AS varchar)) ILIKE UNACCENT('%" . $params . "%')";
                          else
                            $orWhere .= "CAST(" . $Colonnes[$i] . " AS varchar) ILIKE '%" . $params . "%'";
                      }
                      $regleFiltrage[] = "(".$orWhere.")";
                  }
              else {
                  if (defined('UNACCENT_QUERY_PG') && UNACCENT_QUERY_PG === "1")
                    $regleFiltrage[] = "UNACCENT(CAST(" . $Colonnes[$i] . " AS varchar)) ILIKE UNACCENT('%" . $entree['sSearch'] . "%')";
                  else
                    $regleFiltrage[] = "CAST(" . $Colonnes[$i] . " AS varchar) ILIKE '%" . $entree['sSearch'] . "%'";
              }
          }
          if (!empty($regleFiltrage))
              $regleFiltrage = array('('.implode(" OR ", $regleFiltrage).')');
      }

      // Column filtering
      for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
          if ( isset($entree['bSearchable_'.$i]) && $entree['bSearchable_'.$i] == 'true' && $entree['sSearch_'.$i] != '' ) {
              if (mb_strpos($entree['sSearch_' . $i], '|')) {
                  $orWhere = '';
                  foreach (explode('|', $entree['sSearch_' . $i]) as $params) {
                      $orWhere .= $orWhere !== '' ? ' OR ' : '';
                      if (defined('UNACCENT_QUERY_PG') && UNACCENT_QUERY_PG === "1")
                        $orWhere .= "UNACCENT(CAST(" . $Colonnes[$i] . " AS varchar)) ILIKE UNACCENT('%" . $params . "%')";
                      else
                        $orWhere .= "CAST(" . $Colonnes[$i] . " AS varchar) ILIKE '%" . $params . "%'";
                  }
                  $regleFiltrage[] = "(".$orWhere.")";
              }
              else {
                  if (defined('UNACCENT_QUERY_PG') && UNACCENT_QUERY_PG === "1")
                    $regleFiltrage[] = "UNACCENT(CAST(" . $Colonnes[$i] . " AS varchar)) ILIKE UNACCENT('%" . $entree['sSearch_' . $i] . "%')";
                  else
                    $regleFiltrage[] = "CAST(" . $Colonnes[$i] . " AS varchar) ILIKE '%" . $entree['sSearch_' . $i] . "%'";
              }
          }
      }

      if (!empty($regleFiltrage))
          $sWhere = " WHERE ".implode(" AND ", $regleFiltrage);
      else
          $sWhere = "";

      // Condition filter
      if (isset($entree['col']) ) {
          $cnd = $entree['col']."=".$entree['fil'];
          if (empty($sWhere))
              $sWhere=" WHERE $cnd ";
          else
              $sWhere.=" AND $cnd ";
      }

      if (empty($sWhere))
          $sWhere=" WHERE ".$Where;
      else
          $sWhere.=" AND ".$Where;

      // Query build
      $colonnesRequete = array();
      foreach ($Colonnes as $col)
          if ($col != ' ')
              $colonnesRequete[] = $col;

      $sRequete = "SELECT " . str_replace(" , ", " ", implode(", ", $colonnesRequete)) ." FROM  $Table $sWhere $sOrdre $Pagination";

      $rResultat = $this->pdo->query( $sRequete, \PDO::FETCH_ASSOC );

      // Number of rows after filtering
      $sRequete = "SELECT count(*) FROM $Table $sWhere";
      $resultatFiltreTotal = $this->pdo->query( $sRequete, \PDO::FETCH_NUM );
      list($filtreTotal) = $resultatFiltreTotal->fetch();

      // Total rows
      $sRequete = "SELECT COUNT(" . $ColonneIndex . ") FROM " . $Table;
      $resultatTotal = $this->pdo->query( $sRequete, \PDO::FETCH_NUM );
      list($iTotal) = $resultatTotal->fetch();

      // Output
      $chaineRetour = array(
          "sEcho" => intval(@$entree['sEcho']), "iTotalRecords" => $iTotal, "iTotalDisplayRecords" => $filtreTotal, "aaData" => array()
      );

      while ( $aLigne = $rResultat->fetch() ) {
          $ligne = array();
          for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
              if ( $Colonnes[$i] == 'version' )
                  // Formatage spécial pour la colonne "version"
                  $ligne[] = ($aLigne[ $Colonnes[$i] ]=='0') ? '-' : $aLigne[ $Colonnes[$i] ];
              elseif ( $Colonnes[$i] != ' ' )
                  // Formatage général
                  $ligne[] = $aLigne[ $Colonnes[$i] ];
          }
          $chaineRetour['aaData'][] = $ligne;
      }
      return json_encode( $chaineRetour, JSON_PARTIAL_OUTPUT_ON_ERROR );
  }

  /**
   * Method to be used with MariaDB / MySQL (for aggregate functions, use views instead of tables)
   *------------------
   * @param Array
   * @param String
   * @param String
   * @param String
   * @return String
   **/
  public function Query(Array $Colonnes, String $ColonneIndex, String $Table, String $Where = "1")
  {
    mb_internal_encoding('UTF-8');

    // Get user search string
    $entree =& $_GET;

    // Pagination
    $Pagination = "";
    if ( isset( $entree['iDisplayStart'] ) && $entree['iDisplayLength'] != '-1' )
      $Pagination = " LIMIT ".intval( $entree['iDisplayStart'] ).", ".intval( $entree['iDisplayLength'] );

    // Sort
    $regleTri = array();
    if ( isset( $entree['iSortCol_0'] ) ) {
      $colonnesTri = intval( $entree['iSortingCols'] );
      for ( $i=0 ; $i<$colonnesTri ; $i++ ) {
        if ( $entree[ 'bSortable_'.intval($entree['iSortCol_'.$i]) ] == 'true' ) {
          $regleTri[] = "`".$Colonnes[ intval( $entree['iSortCol_'.$i] ) ]."` " .($entree['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
        }
      }
    }

    if (!empty($regleTri)) {
      $sOrdre = " ORDER BY ".implode(", ", $regleTri);
    } else
      $sOrdre = "";

    $nombreColonnes = count($Colonnes);
    if (isset($entree['sSearch']))
      $entree['sSearch'] = str_replace(["'",'"'], "", $entree['sSearch']);

    if ( isset($entree['sSearch']) && $entree['sSearch'] != "" ) {
      $regleFiltrage = array();
      for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
        if ( isset($entree['bSearchable_'.$i]) && $entree['bSearchable_'.$i] == 'true' )
          if (mb_strpos($entree['sSearch'], '|')) {
            $orWhere = '';
            foreach (explode('|', $entree['sSearch']) as $params) {
              $orWhere .= $orWhere !== '' ? ' OR ' : '';
              $orWhere .= "CAST(" . $Colonnes[$i] . " AS CHAR) LIKE '%" . $params . "%'";
            }
            $regleFiltrage[] = "(".$orWhere.")";
          } else {
            $regleFiltrage[] = "`".$Colonnes[$i]."` LIKE '%".$entree['sSearch'] ."%'";
          }
      }
      if (!empty($regleFiltrage))
        $regleFiltrage = array('('.implode(" OR ", $regleFiltrage).')');
    }

    // Columns filtering
    for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
      if ( isset($entree['bSearchable_'.$i]) && $entree['bSearchable_'.$i] == 'true' && $entree['sSearch_'.$i] != '' )
        if (mb_strpos($entree['sSearch_' . $i], '|')) {
          $orWhere = '';
          foreach (explode('|', $entree['sSearch_' . $i]) as $params) {
            $orWhere .= $orWhere !== '' ? ' OR ' : '';
            $orWhere .= "CAST(" . $Colonnes[$i] . " AS CHAR) LIKE '%" . $params . "%'";
          }
          $regleFiltrage[] = "(".$orWhere.")";
        } else {
          $regleFiltrage[] = "`".$Colonnes[$i]."` LIKE '%". $entree['sSearch_'.$i] . "%'";
        }
    }

    if (!empty($regleFiltrage))
      $sWhere = " WHERE ".implode(" AND ", $regleFiltrage);
    else
      $sWhere = "";

    // Condition filter
    if (isset($entree['col']) ) {
      $cnd = $entree['col']."=".$entree['fil'];
      if (empty($sWhere))
        $sWhere=" WHERE $cnd ";
      else
        $sWhere.=" AND $cnd ";
    }

    if (empty($sWhere))
      $sWhere=" WHERE ".$Where;
    else
      $sWhere.=" AND ".$Where;

    // Query build
    $colonnesRequete = array();
    foreach ($Colonnes as $col)
      if ($col != ' ')
        $colonnesRequete[] = $col;

    $sRequete = "
    SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", $colonnesRequete)."` FROM `".$Table."`".$sWhere.$sOrdre.$Pagination;
    $rResultat = $this->pdo->query( $sRequete, \PDO::FETCH_ASSOC );

    // Number of rows after filtering
    $sRequete = "SELECT FOUND_ROWS()";
    $resultatFiltreTotal = $this->pdo->query( $sRequete, \PDO::FETCH_NUM );
    list($filtreTotal) = $resultatFiltreTotal->fetch();

    // Total rows
    $sRequete = "SELECT COUNT(`".$ColonneIndex."`) FROM `".$Table."`";
    $resultatTotal = $this->pdo->query( $sRequete, \PDO::FETCH_NUM );
    list($iTotal) = $resultatTotal->fetch();

    // Output
    $chaineRetour = array(
      "sEcho" => intval(@$entree['sEcho']), "iTotalRecords" => $iTotal, "iTotalDisplayRecords" => $filtreTotal, "aaData" => array()
    );

    while ( $aLigne = $rResultat->fetch() ) {
      $ligne = array();
      for ( $i=0 ; $i<$nombreColonnes ; $i++ ) {
        if ( $Colonnes[$i] == 'version' )
          // Specific format for row column
          $ligne[] = ($aLigne[ $Colonnes[$i] ]=='0') ? '-' : $aLigne[ $Colonnes[$i] ];
        elseif ( $Colonnes[$i] != ' ' )
          // Generic output
          $ligne[] = $aLigne[ $Colonnes[$i] ];
      }
      $chaineRetour['aaData'][] = $ligne;
    }
    return json_encode( $chaineRetour, JSON_PARTIAL_OUTPUT_ON_ERROR );
  }
}
